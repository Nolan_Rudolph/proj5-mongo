Author: Nolan Rudolph  
DuckID: 95155319  
E-mail: ngr@uoregon.edu  

Welcome to my acp times project, part 2!  
  
With this software, you are able to input a starting date, time, and entire race size, and then proceed by implementing different brevet points you will encounter along the race.  

Examples of usage:  
index.html: Input a starting date, time, and race size. This will redirect you to entries.html.  
entries.html: Begin inputting brevet stop points but entering in a name, description, and distance. Click submit to register the brevet point into the database.  
  
Once you are finished adding brevet stops, click "display" in order to view the opening and closing times of the brevet points you have implemented.  

Enjoy!
