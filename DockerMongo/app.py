import os, acp_times
from flask import Flask, redirect, url_for, request, render_template, session
from pymongo import MongoClient

app = Flask(__name__)

app.secret_key = '56721329980543265787809923'
app.config['SESSION_TYPE'] = 'filesystem'

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb


@app.route('/')
def index():
    db.tododb.drop()
    return render_template('index.html')


@app.route('/entries', methods=['GET', 'POST'])
def entries():
    if session.get('total_distance') and session.get('begin_date') and session.get('begin_time'):
        return render_template('entries.html')
    else:
        session['total_distance'] = int(request.form['total_distance'])
        # if not session.get('begin_date'):
        session['begin_date'] = request.form['begin_date']
        # if not session.get('begin_time'):
        session['begin_time'] = request.form['begin_time']
    return render_template('entries.html')


@app.route('/new', methods=['POST'])
def new():
    app.logger.debug("session['begin_date'] = " + session['begin_date'])
    app.logger.debug("session['begin_time'] = " + session['begin_time'])
    app.logger.debug("session['total_distance'] = " + str(session['total_distance']))

    use_date = session['begin_date'] + "T" + session['begin_time']
    distance = int(request.form['distance'])
    total_distance = session['total_distance']

    if distance > total_distance:
        return redirect(url_for('entries'))

    open_time = acp_times.open_time(distance, total_distance, use_date)
    close_time = acp_times.close_time(distance, total_distance, use_date)

    ret_open_time = "{}/{} {}:{}".format(open_time[5:7], open_time[8:10],
                                         open_time[11:13], open_time[14:16])

    ret_close_time = "{}/{} {}:{}".format(close_time[5:7], close_time[8:10],
                                         close_time[11:13], close_time[14:16])

    item_doc = {
        'open_time': ret_open_time,
        'close_time': ret_close_time,
        'name': request.form['name'],
        'description': request.form['description'],
        'distance': request.form['distance']
    }
    db.tododb.insert_one(item_doc)

    app.logger.debug("DATABASE LOGS:")
    _items = db.tododb.find()
    for item in _items:
        app.logger.debug(item)

    return redirect(url_for('entries'))


@app.route('/todo', methods=['POST'])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)